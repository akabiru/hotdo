class HabitsController < ApplicationController
  before_action :set_habit

  def show; end

  def increment
    @habit.update(count: @habit.count + 1)
    redirect_to @habit
  end

  def decrement
    @habit.update(count: @habit.count - 1)
    redirect_to @habit
  end

  private

  def set_habit
    @habit = Habit.find(params['id'])
  end
end
