Rails.application.routes.draw do
  root 'tasks#index'

  resources :habits, only: :show do
    member do
      post :increment
      post :decrement
    end
  end

  resources :tasks do
    post :toggle, on: :member, defaults: { format: :json }
  end
end
